import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    titles = ['Mr.','Mrs.','Miss.']
    results = []

    for title in titles:
        mask = df['Name'].str.contains(title)
        df_title = df[mask]
        missing = df_title['Age'].isnull().sum()
        median = round(df_title['Age'].median())
        df.loc[mask, 'Age'] = df.loc[mask, 'Age'].fillna(median)
        results.append((title, missing, median))

    return results

get_filled()